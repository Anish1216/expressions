/**
 * This program is based on work by Prof. Corin Pitcher.
 * I used interfaces to create a system in which expressions with operations can be evaluated.
 * Expressions make recursive calls when evaluating,
 * and perform similarly when generating a formatted String.
 *  
 * @author AnishKrishnan
 *
 */

interface Expression {
	int evaluate();
	String toString();	// Equivalent to prettyPrint()
}

interface Operation {
	int perform(int x, int y);
	String toString();
}

class OpAdd implements Operation {
	public int perform(int x, int y) {
		return x + y;
	}

	public String toString() {
		return "+";
	}
}

class OpSub implements Operation {
	public int perform(int x, int y) {
		return x - y;
	}

	public String toString() {
		return "-";
	}
}

class OpMul implements Operation {
	public int perform(int x, int y) {
		return x * y;
	}

	public String toString() {
		return "*";
	}
}

class OpDiv implements Operation {
	public int perform(int x, int y) {
		return x / y;
	}

	public String toString() {
		return "/";
	}
}

class ExpInt implements Expression {
	int n;

	ExpInt(int n) {
		this.n = n;
	}

	public int evaluate() {
		return n;
	}

	public String toString() {
		return Integer.toString(n);
	}
}

class ExpOp implements Expression {
	Expression e1, e2;
	Operation op;

	ExpOp(Expression e1, Operation op, Expression e2) {
		this.e1 = e1;
		this.op = op;
		this.e2 = e2;
	}

	public int evaluate() {
		return op.perform(e1.evaluate(), e2.evaluate());
	}

	public String toString() {
		return String.format("(%s %s %s)", e1.toString(), op.toString(), e2.toString());
	}
}

public class ExpressionTest {

	public static void main(String[] args) {

		// (1 + 2) * 3
		Expression exp1 = new ExpOp(
					new ExpOp(
						new ExpInt(1),
						new OpAdd(),
						new ExpInt(2)),
					new OpMul(),
					new ExpInt(3));
		
		// 7 + 7 / 7 + 7 * 7 - 7
		Expression exp2 = new ExpOp(
					new ExpOp(
						new ExpInt(7),
						new OpAdd(),
						new ExpOp(
							new ExpInt(7),
							new OpDiv(),
							new ExpInt(7))),
					new OpAdd(),
					new ExpOp(
						new ExpOp(
							new ExpInt(7),
							new OpMul(),
							new ExpInt(7)),
						new OpSub(),
						new ExpInt(7)));

		System.out.printf("%s = %d\n", exp1.toString(), exp1.evaluate());
		System.out.printf("%s = %d\n", exp2.toString(), exp2.evaluate());
	}
}
